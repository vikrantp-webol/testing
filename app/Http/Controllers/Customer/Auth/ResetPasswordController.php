<?php

namespace App\Http\Controllers\Customer\Auth;
use App\Customer;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    public function showResetForm($confirmation_code) {
        //dd($confirmation_code);
        return view('customer.auth.password.reset')->with('confirmation_code');
    }
    public function reset(Request $request) {
          //dd($request);exit;
          $validData = $this->validate($request, [
            
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            

        ]);
        Customer::where('email',$validData['email'])->update(['password' => Hash::make($validData['password'])]);
        //$product->update($request->all());

    }
}
