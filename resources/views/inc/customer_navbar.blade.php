<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="{{ asset('images/close.png') }}" alt="close_btn"></a>
    <a href="#">Overview</a>
    <a href="#">Redeem voucher</a>
    <a href="#">View/Share listing</a>
    <a href="#">Vouchers sold</a>
    <a href="#">Referral scheme</a>
    <a href="#">Edit listing</a>
    <a href="#">Edit business details</a>
    <a href="#">Edit bank details</a>
    <a href="#">Invoices</a>
    <a href="#">Your account</a>
    <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#customer-logout-form').submit();">
                                Logout
    </a>
    <form id="customer-logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    
  
</div>


<!-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @if(Auth::guard('customer')->check())
                    <li class="nav-item dropdown">
                        <a id="adminDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::guard('customer')->user()->name }} (CUSTOMER) <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customerDropdown">
                            <a href="{{route('customer.home')}}" class="dropdown-item">Dashboard</a>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault();document.querySelector('#customer-logout-form').submit();">
                                Logout
                            </a>
                            <form id="customer-logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.login') }}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('customer.register') }}">Register</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>   -->