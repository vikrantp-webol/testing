<?php

namespace App\Http\Controllers\Customer\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Customer;

class LoginController extends Controller {
    use AuthenticatesUsers;

    public function __construct() {
        $this->middleware('guest:customer')->except('logout');
    }

    public function showLoginForm() {
        return view('customer.auth.login',[
            'title' => 'Customer Login',
            'loginRoute' => 'customer.login',
            'forgotPasswordRoute' => 'customer.password.request',
            'layout' => 'customer_'
        ]);
    }

    public function login(Request $request) {
        $this->validator($request);
        //dd($request->email);
        if(Customer::where('email',$request->email)->where('email_verified' , 1)->exists())
        {
            if(Auth::guard('customer')->attempt($request->only('email','password'),$request->filled('remember'))){
                //Authentication passed...
                return redirect()
                    ->intended(route('customer.home'))
                    ->with('status','You are Logged in as Customer!');
            }
            else{
                //dd("fds");
                return redirect()
                ->back()
                ->withInput()
                ->with('error','Login failed,Please try again');
            }
        }
        else{
            
            return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed,Please verify email');
        }

        //Authentication failed...
        return $this->loginFailed();
    }
    
    public function logout() {
        Auth::guard('customer')->logout();
        return redirect()
            ->route('customer.login')
            ->with('status','Customer has been logged out!');
    }

    private function validator(Request $request) {
        //validate the form...
        $rules = [
            'email'    => 'required|email|exists:customers|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed, please try again!');
    }
}
