@extends('layouts.customer_app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="login_main pt-4">
            <h2 class="page_heading">CREATE YOUR ACCOUNT</h2 class="page_heading">
            <form method="POST" action="{{ route('customer.login') }}" class="form_main pt-3">
                @csrf
                <div class="form-group">
                    <label for="email">EMAIL ADDRESS</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="pwd">PASSWORD</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="pwd" name="password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary submit_btn">CREATE ACCOUNT</button>
                @if (Route::has('customer.password.request'))
                    <a class="btn btn-link" href="{{ route('customer.password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection
