<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){
    Auth::routes();
    Route::get('/', 'HomeController@index')->name('home')->middleware('auth:admin');
});
Route::prefix('/customer')->name('customer.')->namespace('Customer')->group(function(){
    Route::namespace('Auth')->group(function(){
        
        //Login Routes
        Route::get('/register','RegisterController@showRegisterForm')->name('register');
        Route::post('/register','RegisterController@create');

        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('logout');
    
        //Forgot Password Routes
        Route::get('/password/email','ForgotPasswordController@showLinkRequestForm')->name('password.email');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        Route::get('/password/reset','ForgotPasswordController@showResetRequestForm')->name('password.request');
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');
    

        Route::get('register/verify/{confirmationCode}', [
            'as' => 'confirmation_path',
            'uses' => 'RegisterController@confirm'
        ]);
        
    });
    Route::get('/', 'HomeController@index')->name('home')->middleware('auth:customer');
});

Route::prefix('/merchant')->name('merchant.')->namespace('Merchant')->group(function(){
    Route::namespace('Auth')->group(function(){
        
        //Login Routes
        Route::get('/register','RegisterController@showRegisterForm')->name('register');
        Route::post('/register','RegisterController@create');
        Route::get('/confirm/{data}','RegisterController@confirm')->name('register.confirm');
        Route::post('/confirm/{data}','RegisterController@saveMerchant');

        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('logout');
    
        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        
        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');
    
    });
    Route::get('/', 'HomeController@index')->name('home')->middleware('auth:merchant');
});
