@extends('layouts.merchant_app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="login_main pt-4">
            <h2 class="page_heading">WHAT WE CHARGE</h2>
            <div class="information_text pt-3">
                <form method="POST" action="{{ route('merchant.register.confirm', $data) }}" class="form_main pt-3">
                @csrf
                    <div class="information_1">
                        <h6>PROCESSING FEE</h6>
                        <div class="process_fee_info row">
                            <div class="col-lg-2 col-md-3 col-sm-3 col-6">
                                <h3>3%</h3>
                            </div>
                            <div class="col-lg-10 col-md-9 col-sm-9 col-6">
                                <p>of the voucher cost price paid by your customer so that we can pay the credit card processing fees.</p>
                            </div>
                        </div>
                        <h6>UNUSED VOUCHER FEE</h6>
                        <div class="process_fee_info row">
                            <div class="col-lg-2 col-md-3 col-sm-3 col-6">
                                <h3>30%</h3>
                            </div>
                            <div class="col-lg-10 col-md-9 col-sm-9 col-6">
                                <p>of the voucher cost price paid by your customer if the voucher goes unused and becomes expired. This helps us cover the cost of supporting and providing the service you’ll keep the remaining 67%.</p>
                            </div>
                        </div>
                    </div>
                    <div class="information_1">
                        <h4>HOW WE PAY YOU</h4>
                        <p>90% of the voucher value paid by the customer is paid out to you in the next fortnightly payment run after the payment has cleared the credit/debit card payment system.</p>
                        <p>3% of the voucher value paid by the customer is retained by us so that we can pay credit card fees and operate the service for you.</p>
                        <p>7% of the voucher value paid by the customer is paid to you in the next payment run after the voucher is redeemed by the customer. These funds may be used to pay the unused voucher fees and to fund your referral scheme.</p>
                    </div>
                    <div class="information_1">
                        <h4>CONFIRMATION / NEXT STEPS</h4>
                        <p>1. We will now verify your account to make sure someone isn’t trying to impersonate your business. We may give you a call to do this.</p>
                        <p>2. We will then create your voucher listing page and send you by email the link to add to your website and to share on social media with your customers.</p>
                    </div>
                    <div class="information_1">
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-sm-2 col-2">
                                <div class="custom-control custom-checkbox mb-3">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck"></label>
                                </div>
                            </div>
                            <div class="col-lg-11 col-md-11 col-sm-10 col-10">
                                <p>Tick to confirm that you accept the fees above and have read and accept the terms and conditions.</p>
                            </div>
                        </div>
                    </div>
                    <div class="all_btn text-right">
                        <a class="back_step_btn" href="register.html">GO BACK A STEP</a>
                        <button type="submit" class="btn btn-primary submit_btn">CONFIRM & SIGN UP</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection