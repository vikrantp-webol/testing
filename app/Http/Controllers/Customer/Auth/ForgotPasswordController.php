<?php

namespace App\Http\Controllers\Customer\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Mail;

class ForgotPasswordController extends Controller
{
    public function showLinkRequestForm() {
        return view('customer.auth.password.email');
    }
    public function sendResetLinkEmail() {
        $confirmation_code = str_random(30);
        $data = ['confirmation_code' => $confirmation_code];

        Mail::send('email.forgotPasswordVerify', $data, function($message) {
            $message
                ->to(Input::get('email'))
                ->subject('Reset password link');
        });
        return redirect()
        ->back()
        ->withInput()
        ->with('status','Please  verify email for password reset');
    }

    public function confirm($confirmation_code)
    {
        return view('customer.auth.password.reset');
    }
    public function showResetRequestForm(){
        return view('customer.auth.password.email');

    }
    
}
