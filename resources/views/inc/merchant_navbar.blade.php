<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="{{ asset('images/close.png') }}" alt="close_btn"></a>
    <a href="#">Overview</a>
    <a href="#">Redeem voucher</a>
    <a href="#">View/Share listing</a>
    <a href="#">Vouchers sold</a>
    <a href="#">Referral scheme</a>
    <a href="#">Edit listing</a>
    <a href="#">Edit business details</a>
    <a href="#">Edit bank details</a>
    <a href="#">Invoices</a>
    <a href="#">Your account</a>
    <a href="#">Log out</a>
</div>