<?php

namespace App\Http\Controllers\Merchant\Auth;

use App\User;
use App\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/merchant';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:merchant');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        // dd($request);exit;
        $validData = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:merchants',
            'password' => 'required|string|min:6|confirmed',
        ]);
        // $request->request->add(['step' => 'step2']);

        $data['_token'] = $request->_token;
        $data['step'] = 'step2';
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $data['mobile'] = $request->mobile;

        // dd(serialize($validData));
        // dd(route('merchant.register.confirm',json_decode($validData)));
        return redirect()->route('merchant.register.confirm',$data);
        
    }

    public function showRegisterForm() {
        return view('merchant.auth.register');
    }

    public function confirm(Request $request) {
        $data = $request->query();
        return view('merchant.auth.confirm_register', ['data' => $data]);
    }

    protected function saveMerchant(Request $request) {
        // dd($request->query);
        $validData = $this->validate($request->query, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:merchants',
            'password' => 'required|string|min:6',
        ]);

        // $validData = 

        return Merchant::create([
            'name' => $validData['name'],
            'email' => $validData['email'],
            'password' => Hash::make($validData['password']),
        ]);
        // return redirect()->route('merchant.register.confirm',$request->toArray());
        
    }
}
