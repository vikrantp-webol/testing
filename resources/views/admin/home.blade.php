@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!<br  />

                    <input type="text" class="form-control" name="type" id="search_type">
                    <input type="text" class="form-control" name="term" id="search_term">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function activatePlacesSearch() {
        var input = document.getElementById('search_term');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVTM__hvntigxKRv_Z80tB_A9k-kikftw&libraries=places&callback=activatePlacesSearch"></script>
@endsection
