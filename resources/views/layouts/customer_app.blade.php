<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>LSV</title>
    <!-- Normal CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
</head>
<body>

    @include('inc.customer_navbar')
    <div class="wrapper">
        <div class="container">
            <header>
                <div class="row">
                    <div class="header_main">
                        <div class="col-xl-6 col-md-6 col-sm-9 col-9">
                            <div class="logo">
                                <a href="index.html"><img src="{{ asset('images/logo.png') }}" alt="logo_img"></a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6 col-sm-3 col-3 text-right">
                            <div class="open_nav">
                                <span onclick="openNav()">
                                    <img src="{{ asset('images/burger_menu.png') }}" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                @yield('content')
            </main>
        </div>
    </div>

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/34012290d7.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>