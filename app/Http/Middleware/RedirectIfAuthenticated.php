<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /* if (Auth::guard($guard)->check()) {
            return redirect('/home');
        } */

        if (Auth::guard($guard)->check()) {

            if($guard == "admin"){
                return redirect()->route('admin.home');
            } if($guard == "customer"){
                return redirect()->route('customer.home');
            } if($guard == "merchant"){
                return redirect()->route('merchant.home');
            } else {
                return redirect()->route('home');
            }
    
        }

        return $next($request);
    }
}
