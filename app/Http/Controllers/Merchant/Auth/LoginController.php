<?php

namespace App\Http\Controllers\Merchant\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller {
    use AuthenticatesUsers;

    public function __construct() {
        $this->middleware('guest:merchant')->except('logout');
    }

    public function showLoginForm() {
        return view('merchant.auth.login');
    }

    public function login(Request $request) {
        $this->validator($request);
    
        if(Auth::guard('merchant')->attempt($request->only('email','password'),$request->filled('remember'))){
            //Authentication passed...
            return redirect()
                ->intended(route('merchant.home'))
                ->with('status','You are Logged in as Merchant!');
        }

        //Authentication failed...
        return $this->loginFailed();
    }
    
    public function logout() {
        Auth::guard('merchant')->logout();
        return redirect()
            ->route('merchant.login')
            ->with('status','Merchant has been logged out!');
    }

    private function validator(Request $request) {
        //validate the form...
        $rules = [
            'email'    => 'required|email|exists:merchants|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed, please try again!');
    }
}
