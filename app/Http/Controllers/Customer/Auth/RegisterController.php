<?php

namespace App\Http\Controllers\Customer\Auth;

use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Input;
use Auth;
    
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/customer';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
         //dd($request);exit;
        $validData = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:customers',
            'password' => 'required|string|min:6|confirmed',
            'mob_no' => 'required',

        ]);
        $confirmation_code = str_random(30);
         Customer::create([
            'name' => $validData['name'],
            'email' => $validData['email'],
            'password' => Hash::make($validData['password']),
            'mob_no' => $validData['mob_no'],
            'confirmation_code' => $confirmation_code,

        ]);
        $data = ['confirmation_code' => $confirmation_code];

        Mail::send('email.verify', $data, function($message) {
            $message
                ->to(Input::get('email'))
                ->subject('Verify your email address');
        });
        return redirect()
        ->back()
        ->withInput()
        ->with('status','Please  verify email to login');        

    }

    public function confirm($confirmation_code)
    {
        
        if( ! $confirmation_code)
        {
            //throw new InvalidConfirmationCodeException;
            return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed,confirmation code not found');
        }

        $user = Customer::where('confirmation_code',$confirmation_code)->first();
        //dd($confirmation_code);
        if ( ! $user)
        {
            //throw new InvalidConfirmationCodeException;
            return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed,verification link does not  exist');
        }

        $user->email_verified = 1;
        $user->confirmation_code = null;
        $user->save();
            
        //if(Auth::guard('customer')->attempt($request->only('email','password'),$request->filled('remember'))){
            //Authentication passed...
            if(Customer::where('email',$user->email)->where('email_verified', 1)->exists())
            {
                Auth::guard('customer')->loginUsingId($user->id);

                return redirect()
                ->intended(route('customer.home'))
                ->with('status','You are Logged in as Customer!');
            }
            else{
                return redirect()
                ->back()
                ->withInput()
                ->with('error','Login failed,Please verify email');
            }
           
    }

    public function showRegisterForm() {
        return view('customer.auth.register');
    }
}
